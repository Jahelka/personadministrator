DROP ALL OBJECTS DELETE FILES;

CREATE SCHEMA IF NOT EXISTS personadministrator;
SET SCHEMA personadministrator;

-- Create Table

-- TestPerson
CREATE TABLE TestPerson (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    surname VARCHAR(255) NOT NULL,
    identificationNumber VARCHAR(10) NOT NULL
);