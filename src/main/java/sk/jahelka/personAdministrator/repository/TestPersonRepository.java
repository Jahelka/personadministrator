package sk.jahelka.personAdministrator.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import sk.jahelka.personAdministrator.model.TestPerson;

public interface TestPersonRepository extends JpaRepository<TestPerson, Long>, JpaSpecificationExecutor<TestPerson> {
    // Custom queries for searching
}