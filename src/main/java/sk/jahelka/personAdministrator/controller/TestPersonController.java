package sk.jahelka.personAdministrator.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import java.util.Optional;

import javax.transaction.Transactional;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import sk.jahelka.personAdministrator.model.TestPerson;
import sk.jahelka.personAdministrator.service.TestPersonService;

@RestController
@Validated
@RequestMapping("/api/testpersons")
@Api(tags = "Test Person Controller", description = "Operations related to managing persons")
public class TestPersonController {

	
    private TestPersonService testPersonService;
    
    @Autowired
	public TestPersonController(TestPersonService service) {
		this.testPersonService = service;
	}
    
    @GetMapping("/{id}")
    @ApiOperation(value = "Get a person by person ID", response = TestPerson.class)
    public ResponseEntity<?> getTestPersonById(
    		@ApiParam(value = "ID of the person", example = "1", required = true)
    		@PathVariable Long id) {
    	return testPersonService.getTestPersonById(id)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }
    
    @PostMapping("/search")
    @ApiOperation(value = "Search persons", response = List.class)
    public List<TestPerson> search(@RequestBody TestPerson searchCriteria) {
        return testPersonService.search(searchCriteria);
    }
    
    @Transactional
    @PatchMapping("/update/{id}")
    @ApiOperation(value = "Update a person by param", response = TestPerson.class)
    public ResponseEntity<TestPerson> update(
            @PathVariable Long id,
            @RequestBody TestPerson updateCriteria) {
        
        Optional<TestPerson> updatedPerson = testPersonService.update(id, updateCriteria);

        return updatedPerson
                .map(person -> ResponseEntity.ok().body(person))
                .orElse(ResponseEntity.notFound().build());
    }

   
}