package sk.jahelka.personAdministrator.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "TestPerson")
@XmlRootElement(name = "testPerson")
@XmlAccessorType(XmlAccessType.FIELD)
public class TestPerson {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @NotBlank(message = "name cannot be blank")
    @JsonProperty("name")
    private String name;
    
    @NotBlank(message = "surname cannot be blank")
    @JsonProperty("surname")
    private String surname;
    
    @NotNull(message = "identificationNumber cannot be null")
    @JsonProperty("identificationNumber")
    private String identificationNumber;
    
    @JsonProperty("year")
    private String year;
    
    @JsonProperty("year-month")
    private String yearMonth;
    
    @JsonProperty("year-month-day")
    private String YearMonthDay;
    
    
    

	public TestPerson(Long id, @NotBlank(message = "name cannot be blank") String name,
			@NotBlank(message = "surname cannot be blank") String surname,
			@NotNull(message = "identificationNumber cannot be null") String identificationNumber) {
		super();
		this.id = id;
		this.name = name;
		this.surname = surname;
		this.identificationNumber = identificationNumber;
	}

	public TestPerson() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getIdentificationNumber() {
		return identificationNumber;
	}

	public void setIdentificationNumber(String identificationNumber) {
		this.identificationNumber = identificationNumber;
	}
	
	

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getYearMonth() {
		return yearMonth;
	}

	public void setYearMonth(String yearMonth) {
		this.yearMonth = yearMonth;
	}

	public String getYearMonthDay() {
		return YearMonthDay;
	}

	public void setYearMonthDay(String yearMonthDay) {
		YearMonthDay = yearMonthDay;
	}

	@Override
	public String toString() {
		return "TestPerson [id=" + id + ", name=" + name + ", surname=" + surname + ", identificationNumber="
				+ identificationNumber + "]";
	}  
}