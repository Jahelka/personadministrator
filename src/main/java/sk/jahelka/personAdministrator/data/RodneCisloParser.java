package sk.jahelka.personAdministrator.data;

import java.time.LocalDate;
import java.text.ParseException;
import java.time.DateTimeException;

public class RodneCisloParser {
	
	private LocalDate datum;
    private String koncovka;
    private boolean muz;
    private String surovaReprezentaciaBezLomky;

    public RodneCisloParser(String retazecSRodnymCislom) throws ParseException {
        if (retazecSRodnymCislom == null) {
            throw new IllegalArgumentException("Rodne cislo nesmie byt null");
        }
        if (retazecSRodnymCislom.length() <= 9) {
            throw new IllegalArgumentException("Rodne cislo je prikratke");
        }
        StringBuilder buffer = new StringBuilder(retazecSRodnymCislom);
        if (buffer.charAt(5) == '/') {
            buffer.delete(5, 5);
        }
        this.datum = parsujDatum(buffer);
        this.muz = parsujPohlavie(buffer);
        this.koncovka = parsujKoncovku(buffer);
        surovaReprezentaciaBezLomky = buffer.toString();
    }

    private String parsujKoncovku(StringBuilder buffer) {
        return buffer.substring(5);
    }

    private boolean parsujPohlavie(StringBuilder buffer) {
        char priznakPohlavia = buffer.charAt(2);
        return priznakPohlavia == '0' || priznakPohlavia == '1';
    }

    public LocalDate parsujDatum(StringBuilder buffer) throws ParseException {
        try {
            return LocalDate.of(parsujRok(buffer), parsujMesiac(buffer), parsujDen(buffer));
        } catch (DateTimeException e) {
            throw new ParseException("Nespravny format datumu", 0);
        }
    }

    public int parsujRok(StringBuilder buffer) throws ParseException {
        try {
            return Integer.parseInt(buffer.substring(0, 2));
        } catch (NumberFormatException e) {
            throw new ParseException("Rok musi byt cislo", 0);
        }
    }

    public int parsujMesiac(StringBuilder buffer) throws ParseException {
        try {
            String mesiacString = buffer.substring(2, 4);
            if (mesiacString.startsWith("0")) {
                mesiacString = mesiacString.substring(1);
            }
            int mesiac = Integer.parseInt(mesiacString);
            // verzia pre zeny
            if (mesiac >= 50) {
                mesiac = mesiac - 50;
            }
            return mesiac;
        } catch (NumberFormatException e) {
            throw new ParseException("Mesiac musi byt cislo", 0);
        }
    }

    private int parsujDen(StringBuilder buffer) throws ParseException {
        try {
            String denString = buffer.substring(4, 6);
            if (denString.startsWith("0")) {
                denString = denString.substring(1);
            }
            return Integer.parseInt(denString);
        } catch (NumberFormatException e) {
            throw new ParseException("Den musi byt cislo", 0);
        }
    }

    public LocalDate dajDatumNarodenia() {
        return this.datum;
    }

    public boolean jeMuž() {
        return this.muz;
    }

    public String toString() {
        return new StringBuilder(this.surovaReprezentaciaBezLomky)
                .insert(5, '/')
                .toString();
    }

    public String toStringBezLomky() {
        return this.surovaReprezentaciaBezLomky;
    }

    public static boolean jeValidne(String retazecSRodnymCislom) {
        try {
            new RodneCisloParser (retazecSRodnymCislom);
            return true;
        } catch (ParseException e) {
            return false;
        }
    }

}
