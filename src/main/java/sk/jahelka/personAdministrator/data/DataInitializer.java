package sk.jahelka.personAdministrator.data;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import sk.jahelka.personAdministrator.model.TestPerson;
import sk.jahelka.personAdministrator.service.TestPersonService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;
import java.io.File;
import java.util.List;

@Component
@DependsOn("testPersonService")
public class DataInitializer implements CommandLineRunner {

    private final TestPersonService testPersonService;
    
    @Autowired
    public DataInitializer(TestPersonService testPersonService) {
        this.testPersonService = testPersonService;
    }

    @Override
    public void run(String... args) throws Exception {
    	
    	 // JSON file import
        String jsonFilePath = "src/main/resources/json/TestPerson.json";

        // Jackson ObjectMapper to read JSON file
        ObjectMapper objectMapper = new ObjectMapper();
        List<TestPerson> jsonPersons = objectMapper.readValue(new File(jsonFilePath), new TypeReference<List<TestPerson>>() {});
        
        // Add  TestPerson to the database
      for (TestPerson testPerson : jsonPersons) { 
    	  testPersonService.addTestPerson(testPerson);
      }
    }
}