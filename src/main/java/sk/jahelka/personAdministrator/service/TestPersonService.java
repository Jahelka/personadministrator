package sk.jahelka.personAdministrator.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import sk.jahelka.personAdministrator.data.RodneCisloParser;
import sk.jahelka.personAdministrator.model.TestPerson;
import sk.jahelka.personAdministrator.repository.TestPersonRepository;

import javax.persistence.criteria.Predicate;
import javax.transaction.Transactional;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class TestPersonService {
    @Autowired
    private TestPersonRepository repository;

    public List<TestPerson> search(TestPerson searchCriteria) 
    {
    	
        Specification<TestPerson> specification = (root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();

            if (searchCriteria.getId() != null) {
                predicates.add(criteriaBuilder.equal(root.get("id"), searchCriteria.getId()));
            }

            if (!StringUtils.isEmpty(searchCriteria.getName())) {
                predicates.add(criteriaBuilder.like(root.get("name"), "%" + searchCriteria.getName() + "%"));
            }
            
            if (!StringUtils.isEmpty(searchCriteria.getSurname())) {
                predicates.add(criteriaBuilder.like(root.get("surname"), "%" + searchCriteria.getSurname() + "%"));
            }
            if (!predicates.isEmpty()) 
            	return criteriaBuilder.or(predicates.toArray(new Predicate[0]));
                
            return criteriaBuilder.and(criteriaBuilder.isNull(root.get("id")));
        };
        
        
        List<TestPerson> result = new ArrayList<>();
//year
        if (!StringUtils.isEmpty(searchCriteria.getYear())) {
        
            for (TestPerson person : repository.findAll()) {
            	RodneCisloParser rodneCisloParser = null;
            	String rodneCislo = person.getIdentificationNumber();
            	
            	try {
            		rodneCisloParser = new RodneCisloParser(rodneCislo);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

            	DateTimeFormatter formatterY = DateTimeFormatter.ofPattern("yyyy");
            	LocalDate datumNarodenia = rodneCisloParser.dajDatumNarodenia();
            	String year = datumNarodenia.format(formatterY);
            	
            	if(searchCriteria.getYear().equals("19" + year.substring(2))) {
            		result.add(person);
            	}
            }         
        }
        
      //year-month
        if (!StringUtils.isEmpty(searchCriteria.getYearMonth())) {
        
            for (TestPerson person : repository.findAll()) {
            	RodneCisloParser rodneCisloParser = null;
            	String rodneCislo = person.getIdentificationNumber();
            	
            	try {
            		rodneCisloParser = new RodneCisloParser(rodneCislo);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

            	DateTimeFormatter formatterYM = DateTimeFormatter.ofPattern("yyyy-MM");
            	LocalDate datumNarodenia = rodneCisloParser.dajDatumNarodenia();
            	String yearMonth = datumNarodenia.format(formatterYM);
            	
            	if(searchCriteria.getYearMonth().equals("19" + yearMonth.substring(2))) {
            		result.add(person);
            	}
            }         
        }
        
      //year-month-day
        if (!StringUtils.isEmpty(searchCriteria.getYearMonthDay())) {
        
            for (TestPerson person : repository.findAll()) {
            	RodneCisloParser rodneCisloParser = null;
            	String rodneCislo = person.getIdentificationNumber();
            	
            	try {
            		rodneCisloParser = new RodneCisloParser(rodneCislo);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

            	DateTimeFormatter formatterYMD = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            	LocalDate datumNarodenia = rodneCisloParser.dajDatumNarodenia();
            	String yearMonthDay = datumNarodenia.format(formatterYMD);
            	
            	if(searchCriteria.getYearMonthDay().equals("19" + yearMonthDay.substring(2))) {
            		result.add(person);
            	}
            }         
        }

        result.addAll(repository.findAll(specification));
        Set<TestPerson> uniquePersons = new HashSet<>(result);
        result = new ArrayList<>();
        result.addAll(uniquePersons);
        return result;
    }
    

	
    public Optional<TestPerson> update(Long id, TestPerson updateCriteria) {
        Optional<TestPerson> optionalTestPerson = repository.findById(id);

        if (optionalTestPerson.isPresent()) {
            TestPerson testPerson = optionalTestPerson.get();

            if (updateCriteria.getName() != null) {
                testPerson.setName(updateCriteria.getName());
            }

            if (updateCriteria.getSurname() != null) {
                testPerson.setSurname(updateCriteria.getSurname());
            }

            return Optional.of(repository.save(testPerson));
        } else {
            return Optional.empty();
        }
    }
    
    public Optional<TestPerson> getTestPersonById(Long id) {
        return repository.findById(id);
    }
    
    public TestPerson addTestPerson(TestPerson testPerson) {
        return repository.save(testPerson);
    }
}